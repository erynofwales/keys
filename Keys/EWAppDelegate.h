//
//  EWAppDelegate.h
//  Keys
//
//  Created by Eryn Wells on 2014-06-13.
//  Copyright (c) 2014 Eryn Wells. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class EWLEDView;


@interface EWAppDelegate : NSObject <NSApplicationDelegate, NSTableViewDataSource, NSTableViewDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSScrollView *keysTableScrollView;
@property (assign) IBOutlet NSTableView *keysTable;

@property (assign) IBOutlet EWLEDView *controlLED;
@property (assign) IBOutlet EWLEDView *optionLED;
@property (assign) IBOutlet EWLEDView *commandLED;
@property (assign) IBOutlet EWLEDView *shiftLED;
@property (assign) IBOutlet EWLEDView *capsLockLED;

@property (readonly, copy) NSString *inputSourceName;
@property (readonly, strong) NSMutableArray *keys;

@end
