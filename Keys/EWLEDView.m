//
//  EWLEDView.m
//  Keys
//
//  Created by Eryn Wells on 2014-06-18.
//  Copyright (c) 2014 Eryn Wells. All rights reserved.
//

#import "EWLEDView.h"

@implementation EWLEDView

@synthesize on = mOn;

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)setOn:(BOOL)on
{
   mOn = on;
   [self setNeedsDisplay:YES];
}

- (void)drawRect:(NSRect)dirtyRect
{
   NSBezierPath *path = [NSBezierPath bezierPathWithOvalInRect:NSInsetRect(self.bounds, 1.0, 1.0)];
   path.lineWidth = 1.0;

   if (self.on) {
      [[NSColor greenColor] set];
   } else {
      [[NSColor clearColor] set];
   }
   [path fill];

   [[NSColor blackColor] set];
   [path stroke];
}

@end
