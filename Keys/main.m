//
//  main.m
//  Keys
//
//  Created by Eryn Wells on 2014-06-13.
//  Copyright (c) 2014 Eryn Wells. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
   return NSApplicationMain(argc, argv);
}
