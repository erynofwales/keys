//
//  EWLEDView.h
//  Keys
//
//  Created by Eryn Wells on 2014-06-18.
//  Copyright (c) 2014 Eryn Wells. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface EWLEDView : NSView

@property (assign, nonatomic) BOOL on;

@end
