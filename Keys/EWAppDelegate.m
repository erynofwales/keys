//
//  EWAppDelegate.m
//  Keys
//
//  Created by Eryn Wells on 2014-06-13.
//  Copyright (c) 2014 Eryn Wells. All rights reserved.
//

#import <Carbon/Carbon.h>
#import <IOKit/hid/IOHIDKeys.h>
#import <IOKit/hid/IOHIDManager.h>

#import "EWAppDelegate.h"
#import "EWLEDView.h"


static BOOL usageIsRealKey(UInt16 usage);
static BOOL usageIsReserved(UInt16 usage);
static void IOHIDManager_ValueCallback(void *context, IOReturn result, void *sender, IOHIDValueRef value);


static NSString *const VMStateKey = @"state";
static NSString *const VMPageKey = @"page";
static NSString *const VMUsageKey = @"usage";
static NSString *const VMNameKey = @"name";


@interface EWAppDelegate ()
{
   IOHIDManagerRef hidManager;
}

@property (weak) IBOutlet NSArrayController *keysController;
@property (readwrite, copy) NSString *inputSourceName;
@property (readwrite, strong) NSMutableArray *keys;

@property (retain) NSEvent *globalEventMonitor;
@property (retain) NSEvent *localEventMonitor;

@end


@implementation EWAppDelegate


+ (NSString *)inputSourceName
{
   TISInputSourceRef inputSourceRef = TISCopyCurrentKeyboardInputSource();
   assert(inputSourceRef);
   return (__bridge NSString *)(TISGetInputSourceProperty(inputSourceRef, kTISPropertyInputSourceID));
}

#pragma mark app delegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
   [[NSDistributedNotificationCenter defaultCenter] addObserver:self selector:@selector(onInputSourceChanged:) name:(NSString *)kTISNotifySelectedKeyboardInputSourceChanged object:nil];

   self.keys = [NSMutableArray array];
   [self startHIDManager];
   [self installEventMonitors];
}


- (void)applicationWillTerminate:(NSNotification *)notification
{
   [self uninstallEventMonitors];
   [self stopHidManager];
}

#pragma mark hid manager

- (void)startHIDManager
{
   IOReturn ioReturn;

   // Create (and validate) an I/O Kit HID manager
   hidManager = IOHIDManagerCreate(kCFAllocatorDefault, kIOHIDManagerOptionNone);
   assert(hidManager);

   // Set up a device matching dictionary and apply it to the manager.
   NSDictionary *matching = @{@kIOHIDDeviceUsagePageKey: @1, @kIOHIDDeviceUsageKey: @6};
   IOHIDManagerSetDeviceMatching(hidManager, (__bridge CFDictionaryRef)(matching));

   // Open the manager instance.
   ioReturn = IOHIDManagerOpen(hidManager, kIOHIDOptionsTypeNone);
   if (ioReturn != kIOReturnSuccess) {
      NSLog(@"Couldn't open IOHIDManager instance.");
      CFRelease(hidManager);
      hidManager = NULL;
      return;
   }

   IOHIDManagerRegisterInputValueCallback(hidManager, IOHIDManager_ValueCallback, (__bridge void *)(self));
   IOHIDManagerScheduleWithRunLoop(hidManager, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
}


- (void)stopHidManager
{
   if (hidManager) {
      IOHIDManagerClose(hidManager, kIOHIDOptionsTypeNone);
      CFRelease(hidManager);
      hidManager = NULL;
   }
}

#pragma mark event handlers

- (void)installEventMonitors
{
   void (^globalEventHandler)(NSEvent *) = ^(NSEvent *event) {
      const NSUInteger flags = [event modifierFlags];
      self.controlLED.on = (flags & NSControlKeyMask) > 0;
      self.optionLED.on = (flags & NSAlternateKeyMask) > 0;
      self.commandLED.on = (flags & NSCommandKeyMask) > 0;
      self.shiftLED.on = (flags & NSShiftKeyMask) > 0;
      self.capsLockLED.on = (flags & NSAlphaShiftKeyMask) > 0;
   };

   NSEvent *(^localEventHandler)(NSEvent *) = ^(NSEvent *event) {
      globalEventHandler(event);
      return event;
   };

   self.globalEventMonitor = [NSEvent addGlobalMonitorForEventsMatchingMask:NSEventMaskFromType(NSFlagsChanged) handler:globalEventHandler];
   self.localEventMonitor = [NSEvent addLocalMonitorForEventsMatchingMask:NSEventMaskFromType(NSFlagsChanged) handler:localEventHandler];
}


- (void)uninstallEventMonitors
{
   if (self.globalEventMonitor) {
      [NSEvent removeMonitor:self.globalEventMonitor];
   }
   if (self.localEventMonitor) {
      [NSEvent removeMonitor:self.localEventMonitor];
   }
}

#pragma mark ui

- (void)awakeFromNib
{
   self.inputSourceName = [[self class] inputSourceName];
}


- (void)onInputSourceChanged: (NSNotification *)note
{
   self.inputSourceName = [[self class] inputSourceName];
}

#pragma mark hid monitor

- (void)processHIDValue: (IOHIDValueRef)value
{
   IOHIDElementRef element = IOHIDValueGetElement(value);
   assert(element);

   UInt32 page = IOHIDElementGetUsagePage(element);
   UInt32 usage = IOHIDElementGetUsage(element);

   if (page != 7) {
      return;
   }

   if (usageIsReserved(usage) || !usageIsRealKey(usage)) {
      return;
   }

   NSString *state = IOHIDValueGetIntegerValue(value) == 1 ? @"Down" : @"Up";

   NSDictionary *keyItem = @{@"state": state, @"page": @(page), @"usage": @(usage), @"name": @""};
   [self.keys addObject:keyItem];
   [self.keysTable reloadData];
   [self scrollKeysTableToBottom];
}

#pragma mark actions

- (IBAction)clear: (id)sender
{
   [self.keys removeAllObjects];
   [self.keysTable reloadData];
}

#pragma mark table view data source

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
   if (tableView != self.keysTable) {
      return 0;
   }
   return self.keys.count;
}


- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
   if (tableView != self.keysTable) {
      return nil;
   }

   NSString *tableColumnID = tableColumn.identifier;
   NSTableCellView *cell = [tableView makeViewWithIdentifier:tableColumnID owner:self];

   if ([tableColumnID isEqualToString:VMStateKey]) {
      cell.textField.stringValue = self.keys[row][VMStateKey];
   }
   else if ([tableColumnID isEqualToString:VMPageKey]) {
      cell.textField.stringValue = self.keys[row][VMPageKey];
   }
   else if ([tableColumnID isEqualToString:VMUsageKey]) {
      cell.textField.stringValue = self.keys[row][VMUsageKey];
   }
   else if ([tableColumnID isEqualToString:VMNameKey]) {
      cell.textField.stringValue = self.keys[row][VMNameKey];
   }

   return cell;
}


- (void)scrollKeysTableToBottom
{
   NSPoint newScrollOrigin;

   if ([self.keysTableScrollView.documentView isFlipped]) {
      newScrollOrigin = NSMakePoint(0.0, NSMaxY([self.keysTableScrollView.documentView frame]) - NSHeight(self.keysTableScrollView.contentView.bounds));
   } else {
      newScrollOrigin = NSMakePoint(0, 0);
   }

   [self.keysTableScrollView.documentView scrollPoint:newScrollOrigin];
}


@end


BOOL
usageIsReserved(UInt16 usage)
{
   return    (usage >= 0x00a5 && usage <= 0x00cf)
          || (usage >= 0x00de && usage <= 0x00df)
          || (usage >= 0x00e8 && usage <= 0xffff);
}


BOOL
usageIsRealKey(UInt16 usage)
{
   return !(usage >= 0x0000 && usage <= 0x0003);
}


void
IOHIDManager_ValueCallback(void *context,
                           IOReturn result,
                           void *sender,
                           IOHIDValueRef value)
{
   EWAppDelegate *appDelegate = (__bridge EWAppDelegate *)context;
   [appDelegate processHIDValue:value];
}