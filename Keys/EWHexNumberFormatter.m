//
//  EWHexNumberFormatter.m
//  Keys
//
//  Created by Eryn Wells on 2014-07-02.
//  Copyright (c) 2014 Eryn Wells. All rights reserved.
//

#import "EWHexNumberFormatter.h"

@implementation EWHexNumberFormatter

- (BOOL)getObjectValue:(out __autoreleasing id *)obj
             forString:(NSString *)string
      errorDescription:(out NSString *__autoreleasing *)error
{
   *obj = [self numberFromString:string];
   return YES;
}

- (NSString *)stringFromNumber:(NSNumber *)number
{
   return [NSString stringWithFormat:@"0x%lX", (long)[number integerValue]];
}

- (NSNumber *)numberFromString:(NSString *)string
{
   unsigned int value;
   [[NSScanner scannerWithString:string] scanHexInt:&value];
   return @(value);
}

@end
